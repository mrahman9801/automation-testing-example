package com.generic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.pageobjectmodels.LoginPage;
import com.utils.BaseConfig;

public class Login {

	public void getLogin() throws Exception {

		WebDriver driver = new ChromeDriver();
		driver.manage().window().fullscreen();
		
		// Find login page
		driver.get(BaseConfig.getConfig("URL"));
		driver.findElement(LoginPage.signIn).click();

		// Put credentials
		driver.findElement(LoginPage.email).click();
		driver.findElement(LoginPage.email).sendKeys(BaseConfig.getConfig("email"));
		driver.findElement(LoginPage.password).click();
		driver.findElement(LoginPage.password).sendKeys(BaseConfig.getConfig("passWord"));

		driver.findElement(LoginPage.finalsignIn).click();
		
		// Need to end session
		driver.quit();
	}

}
