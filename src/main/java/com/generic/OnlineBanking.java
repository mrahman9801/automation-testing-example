package com.generic;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.pageobjectmodels.OnlineBankingLoginPage;
import com.utils.BaseConfig;

public class OnlineBanking {

	@Test
	public static void getLogin() throws Exception {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(BaseConfig.getConfig("URL"));

		driver.findElement(OnlineBankingLoginPage.getUser()).click();
		driver.findElement(OnlineBankingLoginPage.getUser()).sendKeys(BaseConfig.getConfig("USER"));
		driver.findElement(OnlineBankingLoginPage.getPassword()).click();
		driver.findElement(OnlineBankingLoginPage.getPassword()).sendKeys(BaseConfig.getConfig("PASSWORD"));
		driver.findElement(OnlineBankingLoginPage.getLogin()).click();

		SoftAssert sf = new SoftAssert();
		sf.assertTrue(driver.findElement(OnlineBankingLoginPage.getLogOutBtn()).isDisplayed());
		sf.assertAll();
		
	}

}