package com.generic;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.asserts.SoftAssert;

import com.pageobjectmodels.OnlineBankingLoginPage;
import com.utils.BaseConfig;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Stepdef {
	WebDriver driver;

	@Given("Go to application URL")
	public void go_to_application_url() throws Exception {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(BaseConfig.getConfig("URL"));
	}

	@Given("put valid user name")
	public void put_valid_user_name() throws Exception {
		driver.findElement(OnlineBankingLoginPage.getUser()).click();
		driver.findElement(OnlineBankingLoginPage.getUser()).sendKeys(BaseConfig.getConfig("USER"));
	}

	@Given("put valid password")
	public void put_valid_password() throws Exception {
		driver.findElement(OnlineBankingLoginPage.getPassword()).click();
		driver.findElement(OnlineBankingLoginPage.getPassword()).sendKeys(BaseConfig.getConfig("PASSWORD"));
	}

	@Given("click login")
	public void click_login() {
		driver.findElement(OnlineBankingLoginPage.getLogin()).click();
	}

	@Then("Logout button should visible for successful login")
	public void logout_button_should_visible_for_successful_login() {
		SoftAssert sf = new SoftAssert();
		sf.assertTrue(driver.findElement(OnlineBankingLoginPage.getLogOutBtn()).isDisplayed());
		sf.assertAll();
	}

	@Given("put invalid user name")
	public void put_invalid_user_name() throws Exception {
		driver.findElement(OnlineBankingLoginPage.getUser()).click();
		driver.findElement(OnlineBankingLoginPage.getUser()).sendKeys(BaseConfig.getConfig("INVALID_USER"));
	}

	@Given("put invalid password")
	public void put_invalid_password() throws Exception {
		driver.findElement(OnlineBankingLoginPage.getPassword()).click();
		driver.findElement(OnlineBankingLoginPage.getPassword()).sendKeys(BaseConfig.getConfig("INVALID_PASSWORD"));
	}

	@Then("Logout button should not visible for successful login")
	public void logout_button_should_not_visible_for_successful_login() {
		Alert alert = driver.switchTo().alert();
		alert.accept();

		SoftAssert sf = new SoftAssert();
		sf.assertTrue(!driver.findElement(OnlineBankingLoginPage.getLogOutBtn()).isDisplayed());
		sf.assertAll();
	}
}
