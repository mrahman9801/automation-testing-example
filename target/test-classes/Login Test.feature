@Smoke @Regression
Feature: Login Test
Description: Testing proper login capability.

@Positive @TC_LF-20
Scenario: Attempt login with valid credentials
Given Go to application URL 
And put valid user name
And put valid password
And click login
Then Logout button should visible for successful login

@Negative @TC_LF-21
Scenario Outline: Attempt login with valid credentials
Given Go to application URL 
And put invalid user name
And put invalid password
And click login
Then Logout button should not visible for successful login
